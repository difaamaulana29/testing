import pyshorteners

def shorten_url(link):
    short = pyshorteners.Shortener()
    shortened_link = short.tinyurl.short(link)
    return shortened_link

# my_url = input("Enter your URL : ")
# print(shorten_url(my_url))